import React, { Component } from 'react';  
import { AppRegistry, FlatList,  
    StyleSheet, Text, View,Alert } from 'react-native';  
import { StatelessList } from './src/components/flatList';
  
export default class FlatListBasics extends Component {  
  
    state = {
        data : [  
            {key: 'Android'},
            {key: 'iOS'}, 
            {key: 'Java'},
            {key: 'Swift'},
            {key: 'Php'},
            {key: 'Hadoop'},
            {key: 'Sap'},  
            {key: 'Python'},
            {key: 'Ajaxs'}, 
            {key: 'C++'},  
            {key: 'Ruby'},
            {key: 'Rail'},
            {key: '.Net'},  
            {key: 'Perl'},
            {key: 'Saps'},
            {key: 'Pythons'},  
            {key: 'Ajax'}, 
            {key: 'C+++'},
            {key: 'Rubys'},  
            {key: 'Rails'},
            {key: '.Nets'},
            {key: '.Netss'},
            {key: '.Netst'},
            {key: '.Netse'},
            {key: 'Perls'}  
        ]
    }
    renderSeparator = () => {  
        return (  
            <View  
                style={{  
                    height: 1,  
                    width: "100%",  
                    backgroundColor: "#000",  
                }}  
            />  
        );  
    };  
    //handling onPress action  
    getListViewItem = (item) => {  
        Alert.alert(item.key);  
    }  
  
    render() {  
        return (  
            <View style={styles.container}>  
                <StatelessList person = {this.state.data}/>
            </View>  
        );  
    }  
}  
  
const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
        marginTop: 16,
        marginStart: 16,
        marginBottom: 16,
        marginEnd: 16
    },  
    item: {  
        padding: 10,  
        fontSize: 18,  
        height: 44,  
    },  
})  
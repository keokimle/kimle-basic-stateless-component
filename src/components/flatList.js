import React from 'react'
import {View, Text, StyleSheet, FlatList} from 'react-native'

const StatelessList = (props) => {
    const { person } = props
    const { text,itemList } = style
    return (
        <View>
            <FlatList 
            data={person}
            renderItem={({item}) =>  
                    <Text style={itemList}>{item.key}</Text>
                }
            />
        </View>
    )
}

const style = StyleSheet.create({
    text : {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'blue'
    },
    itemList: {  
        padding: 10,  
        fontSize: 18,  
        height: 44,  
    }
})

export {StatelessList}